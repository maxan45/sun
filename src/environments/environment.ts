// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  hmr: false,
  firebase: {
    apiKey: 'AIzaSyBRMOaJBuwOZ3yZDP4T_Oh003z9KMPGyx8',
    authDomain: 'suntest-b9196.firebaseapp.com',
    databaseURL: 'https://suntest-b9196.firebaseio.com',
    projectId: 'suntest-b9196',
    storageBucket: '',
    messagingSenderId: '261840848559'
  }
};
