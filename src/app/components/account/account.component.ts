import {Component, OnInit} from '@angular/core';
import {AccountService} from '../../services/account.service';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  value = '';
  filter = '';

  constructor(public accountService: AccountService) {
  }

  ngOnInit() {
  }

  handleEmit(event: object): void {
    this.value = event['value'];
    this.filter = event['filter'];
  }

}
