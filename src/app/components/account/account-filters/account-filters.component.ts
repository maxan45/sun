import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Account, AccountService} from '../../../services/account.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-account-filters',
  templateUrl: './account-filters.component.html',
  styleUrls: ['./account-filters.component.css']
})
export class AccountFiltersComponent implements OnInit {
  @Input() accounts: Account[];
  @Output() filterGate: EventEmitter<object> = new EventEmitter();
  @Output() filterType: EventEmitter<object> = new EventEmitter();
  @Output() filterAccount: EventEmitter<object> = new EventEmitter();
  uniqueGates: string[];
  uniqueTypes: string[];
  uniqueAccounts: string[];

  constructor(public accountService: AccountService) {
    this.uniqueGates = [];
    this.uniqueTypes = [];
    this.uniqueAccounts = [];
  }

  ngOnInit() {
    this.uniqueGates = this.getUniquesValues('GATE');
    this.uniqueTypes = this.getUniquesValues('ACCOUNT_TYPE');
    this.uniqueAccounts = this.getUniquesValues('ACCOUNT');
  }

  emitGate(event: any) {
    this.filterGate.emit({value: event.target.value, filter: 'GATE'});
    $('#type').val('');
    $('#account').val('');
  }

  emitType(event: any) {
    this.filterType.emit({value: event.target.value, filter: 'ACCOUNT_TYPE'});
    $('#gate').val('');
    $('#account').val('');
  }

  emitAccount(event: any) {
    this.filterAccount.emit({value: event.target.value, filter: 'ACCOUNT'});
    $('#type').val('');
    $('#gate').val('');
  }

  private getUniquesValues(value: string) {
    const array = [];
    this.accounts.forEach((x) => array.push(x[value]));
    return array.filter((item, pos) => {
      return array.indexOf(item) === pos;
    });
  }
}
