import {Component, OnInit} from '@angular/core';
import {ActivesService} from '../../services/actives.service';

@Component({
  selector: 'app-actives',
  templateUrl: './actives.component.html',
  styleUrls: ['./actives.component.css']
})
export class ActivesComponent implements OnInit {

  constructor(public activesService: ActivesService) {
  }

  ngOnInit() {
  }

}
