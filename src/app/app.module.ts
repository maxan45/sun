import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {CoreModule} from './core/core.module';
import {AngularFireModule} from 'angularfire2';
import {environment} from '../environments/environment';
import {HomeComponent} from './components/home/home.component';
import {ActivesComponent} from './components/actives/actives.component';
import {AccountComponent} from './components/account/account.component';
import {KeyPipe} from './pipes/key.pipe';
import {ServicesModule} from './services/services.module';
import {MyCurrencyPipe} from './pipes/my-currency.pipe';
import {HttpClientModule} from '@angular/common/http';
import {AccountItemComponent} from './components/account/account-item/account-item.component';
import {AccountFiltersComponent} from './components/account/account-filters/account-filters.component';
import {AccountsPipe} from './pipes/accounts.pipe';
import {UiSwitchModule} from 'ngx-ui-switch';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ActivesComponent,
    AccountComponent,
    KeyPipe,
    MyCurrencyPipe,
    AccountItemComponent,
    AccountFiltersComponent,
    AccountsPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    CoreModule,
    ServicesModule,
    UiSwitchModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
