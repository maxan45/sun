import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Subject} from 'rxjs/Subject';
import {Http} from '@angular/http';
import {HttpClient} from '@angular/common/http';

export class Account {
  ACCOUNT: number;
  ACCOUNT_TYPE: string;
  BALANCE: number;
  CREDIT: number;
  CURRENCY: string;
  EQUITY: number;
  FREE: string;
  GATE: string;
  LEVEL: number;
  LEVERAGE: number;
  PROFIT: number;
}

@Injectable()
export class AccountService {
  dataType$: boolean;
  accounts$: Observable<Account[]>;

  constructor(private afs: AngularFirestore, private http: HttpClient) {
    this.accounts$ = this.http.get<Account[]>('../assets/demo-data/account.json');
  }

  changeDataType(event: boolean): void {
    this.dataType$ = event;
    setTimeout(() => {
      if (event) {
        const collectionActives: AngularFirestoreCollection<Account> = this.afs.collection('account');
        this.accounts$ = collectionActives.valueChanges();
      } else {
        this.accounts$ = this.http.get<Account[]>('../assets/demo-data/account.json');
      }
    }, 1000);
  }
}
