import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActivesService} from './actives.service';
import {AccountService} from './account.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [],
  providers: [
    ActivesService,
    AccountService
  ]
})
export class ServicesModule {
}
