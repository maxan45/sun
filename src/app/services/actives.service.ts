import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';

export class Actives {
  FRIEND: object;
  MONEY_BOX: object;
  PARTNER: object;
  SAFE: object;
  TOTAL: object;
  ACCOUNT: object;
}

@Injectable()
export class ActivesService {
  actives$: Observable<Actives[]>;

  constructor(private afs: AngularFirestore) {
    const collectionActives: AngularFirestoreCollection<Actives> = afs.collection('actives');
    this.actives$ = collectionActives.valueChanges();
  }

}
