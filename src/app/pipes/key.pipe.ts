import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'key'
})
export class KeyPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const keys: any = [];
    if (Object.keys(value)) {
      for (const key in value) {
        if (value.hasOwnProperty(key)) {
          keys.push({key, value: value[key]});
        }
      }
    }
    return keys;
  }

}
