import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'accounts'
})
export class AccountsPipe implements PipeTransform {

  transform(value: any, args?: any, filter?: any): any {
    if (args === '') {
      return value;
    } else {
      return value.filter((x) => x[filter] === (filter !== 'ACCOUNT' ? args : +args));
    }
  }

}
