import {Pipe, PipeTransform} from '@angular/core';

const map = {
  'RUB': '&#8381;',
  'USD': '&#36;',
  'EUR': '&#8364;',
};

@Pipe({
  name: 'myCurrency'
})
export class MyCurrencyPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return map[value];
  }

}
